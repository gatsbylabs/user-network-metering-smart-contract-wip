# Smart Contracts made in Solidity #

### Description ###

This repository is for all versions of the different smart contracts Gatsby might need/use.

### How to Deploy Smart Contracts ###

* Copy code
* Open Etherum 
* Go to Smart Contract
* Deploy Smart Contract
* Paste Code into source code
* Deploy

### Sources used ###
* http://truffleframework.com/ 
* solidity.readthedocs.io
